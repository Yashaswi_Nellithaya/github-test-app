package com.yashaswi.githubapp.network

import com.yashaswi.githubapp.dtos.GitBaseReposDTO
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Yashaswi N P on 1/6/20.
 */
interface ApiService {

    // api endpoint to to get the trending repos
    @GET("developers")
    fun getGitTrendingRepos(
        @Query("language") language: String,
        @Query("amp;since") since: String
    ): Observable<Response<List<GitBaseReposDTO>>>
}