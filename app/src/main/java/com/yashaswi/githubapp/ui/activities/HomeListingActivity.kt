package com.yashaswi.githubapp.ui.activities

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.yashaswi.githubapp.R
import com.yashaswi.githubapp.dtos.GitBaseReposDTO
import com.yashaswi.githubapp.ui.adapters.GitReposAdapter
import com.yashaswi.githubapp.ui.listeners.OnItemClickListener
import com.yashaswi.githubapp.utils.AppConstants
import com.yashaswi.githubapp.utils.SystemUtils
import com.yashaswi.githubapp.viewmodels.GitRepoViewModel
import kotlinx.android.synthetic.main.layout_detail_bottom_sheet.*
import kotlinx.android.synthetic.main.layout_home_content.*
import org.koin.android.viewmodel.ext.android.viewModel
import kotlin.Comparator
import kotlin.collections.ArrayList


class HomeListingActivity : AppCompatActivity() {

    private lateinit var gitReposAdapter: GitReposAdapter
    private val gitRepoViewModel by viewModel<GitRepoViewModel>()
    private var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>? = null
    private var trendingRepos = ArrayList<GitBaseReposDTO>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_listing)
        initView()
    }

    /**
     * initializes the basic views
     */
    private fun initView() {
        progressBar.visibility = View.VISIBLE
        subscribeViewModels()
        SystemUtils.setCustomNavigationBarColor(R.color.colorPrimary, this)
        //RV setUp
        gitReposAdapter = GitReposAdapter(this, onRepositoryItemClickListener)
        homeListingRV.layoutManager = StaggeredGridLayoutManager(2, RecyclerView.VERTICAL)
        homeListingRV.adapter = gitReposAdapter
        //setup searchview
        repoSearchView.setOnQueryTextListener(onSearchQueryTextListener)
        //setup bottom sheet for sort
        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)
        repoSortIB.setOnClickListener {
            showSortBottomSheet()
        }
    }

    /**
     * bottomsheet view for sort
     */
    private fun showSortBottomSheet() {
        if (bottomSheetBehavior?.state != BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
            sortByRepoNameTV.setOnClickListener {
                sortByRepoNameTV.typeface = Typeface.DEFAULT_BOLD
                sortByUserNameTV.typeface = Typeface.DEFAULT
                sortByRankTV.typeface = Typeface.DEFAULT
                trendingRepos.sortWith(Comparator { o1, o2 ->
                    o1?.repo?.name.toString().compareTo(o2?.repo?.name.toString())
                })
                gitReposAdapter.notifyDataSetChanged()
                bottomSheetBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
            }
            sortByRankTV.setOnClickListener {
                sortByRepoNameTV.typeface = Typeface.DEFAULT
                sortByUserNameTV.typeface = Typeface.DEFAULT
                sortByRankTV.typeface = Typeface.DEFAULT_BOLD
                trendingRepos.sortWith(Comparator { o1, o2 ->
                    o2?.rank?.let { it1 -> o1?.rank?.compareTo(it1) } ?: -1
                })
                gitReposAdapter.notifyDataSetChanged()
                bottomSheetBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
            }
            sortByUserNameTV.setOnClickListener {
                sortByRepoNameTV.typeface = Typeface.DEFAULT
                sortByRankTV.typeface = Typeface.DEFAULT
                sortByUserNameTV.typeface = Typeface.DEFAULT_BOLD
                trendingRepos.sortWith(Comparator { o1, o2 ->
                    o1?.username.toString().compareTo(o2?.username.toString())
                })
                gitReposAdapter.notifyDataSetChanged()
                bottomSheetBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
            }
        } else {
            bottomSheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
        }
    }

    /**
     * all the data fetch
     */
    private fun subscribeViewModels() {
        gitRepoViewModel.getTrendingRepositories()
            .observe(this, Observer { response ->
                if (response.isNotEmpty()) {
                    trendingRepos = ArrayList(response)
                    progressBar.visibility = View.GONE
                    gitReposAdapter.setDataAndRefresh(trendingRepos)
                    repoSortIB.visibility = View.VISIBLE
                }
            })
    }

    override fun onBackPressed() {
        if (bottomSheetBehavior?.state == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
        } else
            super.onBackPressed()
    }

    /**
     * on item click listener
     */
    private val onRepositoryItemClickListener: OnItemClickListener<GitBaseReposDTO> =
        object : OnItemClickListener<GitBaseReposDTO> {
            override fun onItemClick(item: GitBaseReposDTO) {
                val intent = Intent(this@HomeListingActivity, GitRepoDetailActivity::class.java)
                intent.putExtra(AppConstants.REPO_DATA, item)
                startActivity(intent)
            }
        }

    /**
     * search query listener for searched key callbacks
     */
    private val onSearchQueryTextListener: androidx.appcompat.widget.SearchView.OnQueryTextListener =
        object : androidx.appcompat.widget.SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                gitReposAdapter.filter.filter(newText)
                return false
            }

        }

}