package com.yashaswi.githubapp.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.yashaswi.githubapp.R
import com.yashaswi.githubapp.dtos.GitBaseReposDTO
import com.yashaswi.githubapp.utils.AppConstants
import kotlinx.android.synthetic.main.activity_git_repo_detail.*

class GitRepoDetailActivity : AppCompatActivity() {
    private var repoDetails: GitBaseReposDTO? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_git_repo_detail)
        initViews()
    }

    private fun initViews() {
        if (extractData()) {
            repoDetailTitleTV.text =
                "#".plus(repoDetails?.rank).plus(" ").plus(repoDetails?.repo?.name)
            repoDetailDescTV.text = "Description: ".plus(repoDetails?.repo?.description)
            repoDetailUrlTV.text = "Visit repository: ".plus(repoDetails?.repo?.url)
            userDetailGitNameTV.text = "User Name: ".plus(repoDetails?.username)
            userDetailNameTV.text = "Name: ".plus(repoDetails?.name)
            userDetailUrlTV.text = "View Profile: ".plus(repoDetails?.url)
            Glide.with(this)
                .load(repoDetails?.avatar)
                .placeholder(R.drawable.placeholder)
                .into(userDetailAvatarIV)
        }
    }

    private fun extractData(): Boolean {
        repoDetails = intent.getSerializableExtra(AppConstants.REPO_DATA) as GitBaseReposDTO
        return repoDetails != null
    }
}