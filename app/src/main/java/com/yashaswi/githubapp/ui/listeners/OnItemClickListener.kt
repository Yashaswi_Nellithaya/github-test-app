package com.yashaswi.githubapp.ui.listeners

/**
 * Created by Yashaswi N P on 1/6/20.
 */
interface OnItemClickListener<T> {
    /**
     * Invokes once the item is clicked
     */
    fun onItemClick(item: T)
}