package com.yashaswi.githubapp.ui.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.yashaswi.githubapp.R
import com.yashaswi.githubapp.dtos.GitBaseReposDTO
import com.yashaswi.githubapp.ui.listeners.OnItemClickListener
import kotlinx.android.synthetic.main.layout_git_repos_item.view.*
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by Yashaswi N P on 1/6/20.
 */

class GitReposAdapter(context: Context, clickListener: OnItemClickListener<GitBaseReposDTO>) :
    RecyclerView.Adapter<GitReposAdapter.GitRepoViewHolder>(), Filterable {
    private val mOnClickListener = clickListener
    private var mContext: Context = context
    var items: ArrayList<GitBaseReposDTO> = ArrayList()
    var fullItems: ArrayList<GitBaseReposDTO> = ArrayList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GitRepoViewHolder {
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.layout_git_repos_item, parent, false)
        return GitRepoViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: GitRepoViewHolder, position: Int) {
        if (items.isNotEmpty()) {
            val repositoryInfo = items[position]
            Glide.with(mContext)
                .load(repositoryInfo.avatar)
                .into(holder.mAuthorAvatarIV)
            holder.mRepoNameTV.text = repositoryInfo.repo?.name
            holder.mAuthorNameTV.text = repositoryInfo.username
            holder.mRepoRankTV.text = "#".plus(repositoryInfo.rank)
        }
    }

    /**
     * to get the data from activities / fragments
     */
    fun setDataAndRefresh(gitReposData: ArrayList<GitBaseReposDTO>) {
        this.items = gitReposData
        fullItems = ArrayList(items)
        notifyDataSetChanged()
    }

    inner class GitRepoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        val mAuthorAvatarIV: ImageView = itemView.repoAuthorAvatarIV
        val mRepoNameTV: TextView = itemView.repoNameTV
        val mAuthorNameTV: TextView = itemView.repoUserNameTV
        val mRepoRankTV: TextView = itemView.repoRankTV

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            mOnClickListener.onItemClick(items[adapterPosition])
        }

    }


    override fun getFilter(): Filter {
        return searchFilter
    }

    /**
     * filter used for search
     */
    private val searchFilter = object : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val filteredList: MutableList<GitBaseReposDTO> = ArrayList()
            if (TextUtils.isEmpty(constraint)) {
                filteredList.addAll(fullItems)
            } else {
                val filterPattern = constraint.toString().toLowerCase(Locale.ROOT).trim()
                for (item in fullItems) {
                    //match for name / username or repository name
                    if (item.username?.toLowerCase(Locale.ROOT)?.contains(filterPattern) == true ||
                        item.name?.toLowerCase(Locale.ROOT)?.contains(filterPattern) == true
                        || item.repo?.name?.toLowerCase(Locale.ROOT)
                            ?.contains(filterPattern) == true
                    ) {
                        filteredList.add(item)
                    }
                }
            }
            val results = FilterResults()
            results.values = filteredList
            return results
        }

        @Suppress("UNCHECKED_CAST")
        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            items.clear()
            items.addAll(results?.values as ArrayList<GitBaseReposDTO>)
            notifyDataSetChanged()
        }

    }

}