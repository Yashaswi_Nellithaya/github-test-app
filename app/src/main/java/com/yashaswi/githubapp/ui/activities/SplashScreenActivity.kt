package com.yashaswi.githubapp.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.yashaswi.githubapp.R
import com.yashaswi.githubapp.utils.SystemUtils

class SplashScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        SystemUtils.setCustomNavigationBarColor(R.color.colorPrimaryDark,
            this)
        SystemUtils.hideBottomNavigationBar(this)
        Handler().postDelayed({
            startActivity(Intent(this, HomeListingActivity::class.java))
            finish()
        }, 1500)
    }
}