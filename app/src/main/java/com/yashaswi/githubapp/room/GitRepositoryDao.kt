package com.yashaswi.githubapp.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.yashaswi.githubapp.dtos.GitBaseReposDTO
import io.reactivex.Completable
import io.reactivex.Flowable

/**
 * Created by Yashaswi N P on 3/6/20.
 */

@Dao
interface GitRepositoryDao {

    @Query("SELECT * from github_repositories")
    fun getAllUserInfo(): Flowable<List<GitBaseReposDTO>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertRepos(repos: List<GitBaseReposDTO>): Completable

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertRepo(repos: GitBaseReposDTO): Completable

}