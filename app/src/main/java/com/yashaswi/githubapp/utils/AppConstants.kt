package com.yashaswi.githubapp.utils

/**
 * Created by Yashaswi N P on 1/6/20.
 */

object AppConstants {
    /* network related constants */
    const val REQUEST_READ_TIME_OUT_IN_SECONDS: Long = 30
    const val REQUEST_WRITE_TIME_OUT_IN_MINUTES: Long = 1
    const val BASE_URL: String = "https://github-trending-api.now.sh/"
    const val LANGUAGE: String = "java"
    const val SINCE: String = "weekly"

    //intent constants
    const val REPO_DATA: String = "REPO_DATA"
}