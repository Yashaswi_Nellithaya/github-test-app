package com.yashaswi.githubapp.dtos

import androidx.room.ColumnInfo
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

/**
 * Created by Yashaswi N P on 1/6/20.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
data class GitReposDTO(

    @ColumnInfo(name = "repo_name")
    @JsonProperty("name")
    var name: String? = null,

    @ColumnInfo(name = "rep_desc")
    @JsonProperty("description")
    var description: String? = null,

    @ColumnInfo(name ="repo_url")
    @JsonProperty("url")
    var url: String? = null
) : Serializable