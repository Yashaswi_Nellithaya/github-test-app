package com.yashaswi.githubapp.dtos

import androidx.annotation.NonNull
import androidx.room.*
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

/**
 * Created by Yashaswi N P on 1/6/20.
 */
@Entity(
    tableName = "github_repositories",
    indices = [Index(value = ["username", "name"], unique = true)]
)
@JsonIgnoreProperties(ignoreUnknown = true)
class GitBaseReposDTO(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo
    @JsonProperty("rank")
    var rank: Int? = null,

    @NonNull
    @ColumnInfo
    @JsonProperty("username")
    var username: String? = null,

    @ColumnInfo
    @JsonProperty("name")
    var name: String? = null,

    @ColumnInfo
    @JsonProperty("url")
    var url: String? = null,

    @ColumnInfo
    @JsonProperty("avatar")
    var avatar: String? = null,

    @Embedded
    @JsonProperty("repo")
    var repo: GitReposDTO? = null

) : Serializable