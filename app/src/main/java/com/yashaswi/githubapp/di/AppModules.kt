package com.yashaswi.githubapp.di

import com.yashaswi.githubapp.network.ApiComponent
import com.yashaswi.githubapp.room.GitHubDatabase
import com.yashaswi.githubapp.utils.SystemUtils
import com.yashaswi.githubapp.viewmodels.GitRepoViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Yashaswi N P on 3/6/20.
 * contains all the koin modules
 */
val applicationModule = module {
    single { ApiComponent }
    single { GitHubDatabase }
    single { SystemUtils }
    viewModel {
        GitRepoViewModel(get())
    }
}