package com.yashaswi.githubapp

import android.app.Application
import com.facebook.stetho.Stetho
import com.yashaswi.githubapp.di.applicationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Created by Yashaswi N P on 3/6/20.
 */


class GitHubTestApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        appplicationInstance = this
        Stetho.initializeWithDefaults(this)
        initKoin()

    }

    /**
     * initialises koin
     */
    private fun initKoin() {
        startKoin {
            androidContext(applicationContext)
            modules(listOf(applicationModule))
        }
    }

    companion object {
        var appplicationInstance: GitHubTestApplication? = null
    }
}