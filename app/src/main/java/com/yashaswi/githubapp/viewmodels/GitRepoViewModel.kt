package com.yashaswi.githubapp.viewmodels

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.yashaswi.githubapp.dtos.GitBaseReposDTO
import com.yashaswi.githubapp.repositories.GitRepoRepository
import com.yashaswi.githubapp.room.GitHubDatabase
import com.yashaswi.githubapp.room.GitRepositoryDao

/**
 * Created by Yashaswi N P on 1/6/20.
 * class is responsible for getting the data from the repository
 * and return to UI
 */
class GitRepoViewModel(context: Context) : ViewModel() {
    private var gitReposRepository: GitRepoRepository
    private var githubDatabase = GitHubDatabase
    private var gitRepositoryDao: GitRepositoryDao

    init {
        gitRepositoryDao = githubDatabase.getDatabase(context.applicationContext).gitRepositoryDao()
        gitReposRepository = GitRepoRepository(gitRepositoryDao)
    }

    /**
     * gets the trending repo list
     */
    fun getTrendingRepositories(): MutableLiveData<List<GitBaseReposDTO>> {
        return gitReposRepository.getGitTrendingRepos()
    }

}