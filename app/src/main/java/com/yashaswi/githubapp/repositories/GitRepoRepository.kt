package com.yashaswi.githubapp.repositories

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.yashaswi.githubapp.dtos.GitBaseReposDTO
import com.yashaswi.githubapp.network.ApiComponent
import com.yashaswi.githubapp.room.GitRepositoryDao
import com.yashaswi.githubapp.utils.AppConstants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by Yashaswi N P on 1/6/20.
 * class is responsible for fetching the data either from api or from db
 *  to be called by viewModel
 */

class GitRepoRepository(repositoryDao: GitRepositoryDao) {
    private val TAG = javaClass.simpleName
    private val apiComponent = ApiComponent
    private lateinit var disposable: Disposable
    private val mRepositoryDao = repositoryDao

    init {
        //can be change to call this weekly basis using workmanager
        fetchTrendingReposFromApi()
    }

    /**
     * returns the data from db
     */
    fun getGitTrendingRepos(): MutableLiveData<List<GitBaseReposDTO>> {
        return fetchHeadlinesFromRoom()
    }

    /**
     * fetched data from local Room db
     */
    private fun fetchHeadlinesFromRoom(): MutableLiveData<List<GitBaseReposDTO>> {
        val repositoryData = MutableLiveData<List<GitBaseReposDTO>>()
        disposable = mRepositoryDao.getAllUserInfo()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                repositoryData.value = it
            }, {
                Log.e(TAG, it.message.toString())
                disposable.dispose()
            })
        return repositoryData
    }


    /**
     * fetches the trending repos list from the api
     */
    private fun fetchTrendingReposFromApi()
            : MutableLiveData<List<GitBaseReposDTO>> {
        val gitRepos = MutableLiveData<List<GitBaseReposDTO>>()
        disposable = apiComponent.getApiInterface()
            .getGitTrendingRepos(AppConstants.LANGUAGE, AppConstants.SINCE)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.isSuccessful) {
                    gitRepos.value = it.body()
                    it.body()?.let { it1 -> addGitReposToRoom(it1) }
                }
            }, {
                gitRepos.value = null
                Log.e(TAG, it.message.toString())
                disposable.dispose()
            })
        return gitRepos
    }


    /**
     * adds data to the room db
     */
    @SuppressLint("CheckResult")
    private fun addGitReposToRoom(repos: List<GitBaseReposDTO>) {
        mRepositoryDao.insertRepos(repos)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.newThread())
            .subscribe {
            }
    }
}